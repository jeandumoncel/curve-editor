function ridges_ravines_2vtk_v2(fileridges,fileravines,vtkfile)

% License & disclaimer.
% ------------------------------------------------------------------------------
%     Copyright 2019 Dumoncel Jean
% ------------------------------------------------------------------------------
%  
% IMPORTANT
%     If you use this program for your publication, please cite:
%       [1] Beaudet, A., Dumoncel, J., de Beer, F., Duployer, B., Durrleman, S., Gilissen, E. P., et al. (2016). 
%           Morphoarchitectural variation in South African fossil cercopithecoid endocasts. Journal of Human Evolution,
%           101, 65-78. doi:10.1016/j.jhevol.2016.09.003
% Example :
% ridges_ravines_2vtk('colored_demi-sphereAmira50000_to_subject_0__t_019_ridges.txt','colored_demi-sphereAmira50000_to_subject_0__t_019_ravines.txt','colored_demi-sphereAmira50000_to_subject_0__t_019.vtk');

[ptsridges sgtsridges RidgenessSphericalnessCyclidenessridges] = ridgesravines2(fileridges);
[ptsravines sgtsravines RidgenessSphericalnessCyclidenessravines] = ridgesravines2(fileravines);

if nargin==3
    [Pts Tri] = VTKPolyDataReader(vtkfile);
end

nbs = unique(ptsridges(:,4));
ptsRidges = [];
for k=1:length(nbs)
    S = sgtsridges(sgtsridges(:,3)==k-1,1:2)+1;
    [val, ~, ~] = unique(S(:));
    d = hist(double(S(:)),double(val));
    vald = val(find(d==1,1,'first'));
    if isempty(vald)
        vald=val(1);
    end
    suite = vald;
    ind = find(S(:,1)==vald|S(:,2)==vald,1,'first');
    valf = setdiff(S(ind,:),vald);
    S(ind,:) = Inf;
    suite(end+1) = valf;
    while ~isempty(ind)
        vald = valf;
        ind = find(S(:,1)==vald|S(:,2)==vald);
        if ~isempty(ind)
            if length(ind)>1
                ind = ind(1);
            end
            valf = setdiff(S(ind,:),vald);
            suite(end+1) = valf;
            S(ind,:) = Inf;
        end
    end
    D = ptsridges(suite,1:3);
    IS = sgtsridges(sgtsridges(:,3)==k-1,4)+1;
    IT = Tri(IS,:);
    ptscmp = Pts(IT(:),:);
    [VD,VI] = pdist2(ptscmp,D,'euclidean','Smallest',1);
    VI(find(diff(VI)==0)+1) = [];
    Dnew = ptscmp(VI,:);
    triRidges{k,1} = [size(ptsRidges,1)+1:size(ptsRidges,1)+size(Dnew,1)]-1;
    ptsRidges = [ptsRidges;Dnew];
end

nbs = unique(ptsravines(:,4));
ptsRavines = [];
triRavines = [];
for k=1:length(nbs)
    S = sgtsravines(sgtsravines(:,3)==k-1,1:2)+1;
    [val, ~, ~] = unique(S(:));
    d = hist(double(S(:)),double(val));
    vald = val(find(d==1,1,'first'));
    if isempty(vald)
        vald=val(1);
    end
    suite = vald;
    ind = find(S(:,1)==vald|S(:,2)==vald,1,'first');
    valf = setdiff(S(ind,:),vald);
    S(ind,:) = Inf;
    suite(end+1) = valf;
    while ~isempty(ind)
        vald = valf;
        ind = find(S(:,1)==vald|S(:,2)==vald);
        if ~isempty(ind)
            if length(ind)>1
                ind = ind(1);
            end
            valf = setdiff(S(ind,:),vald);
            suite(end+1) = valf;
            S(ind,:) = Inf;
        end
    end
    D = ptsravines(suite,1:3);
    IS = sgtsravines(sgtsravines(:,3)==k-1,4)+1;
    IT = Tri(IS,:);
    ptscmp = Pts(IT(:),:);
    [VD,VI] = pdist2(ptscmp,D,'euclidean','Smallest',1);
    VI(find(diff(VI)==0)+1) = [];
    Dnew = ptscmp(VI,:);
    triRavines{k,1} = [size(ptsRavines,1)+1:size(ptsRavines,1)+size(Dnew,1)]-1;
    ptsRavines = [ptsRavines;Dnew];    
end

Write_vtk_polyline_multi_scalar(ptsRidges.', triRidges,RidgenessSphericalnessCyclidenessridges(:,1),RidgenessSphericalnessCyclidenessridges(:,2),RidgenessSphericalnessCyclidenessridges(:,3),[fileridges(1:end-4) '.vtk'],{'Ridgeness','Sphericalness','Cyclideness'});

Write_vtk_polyline_multi_scalar(ptsRavines.', triRavines,RidgenessSphericalnessCyclidenessravines(:,1),RidgenessSphericalnessCyclidenessravines(:,2),RidgenessSphericalnessCyclidenessravines(:,3),[fileravines(1:end-4) '.vtk'],{'Ridgeness','Sphericalness','Cyclideness'});
