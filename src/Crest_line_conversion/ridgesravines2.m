function [tabPoints tabSegments tabRidgenessSphericalnessCyclideness] = ridgesravines2(filein)

% Copyright Jean Dumoncel

fid = fopen(filein,'r');
NombreDePoints = sscanf(fgetl(fid),'%d');
NombreDeSegments = sscanf(fgetl(fid),'%d');
NombreDeLignes = sscanf(fgetl(fid),'%d');

tabPoints = fscanf(fid,'%f %f %f %d\n',[4,NombreDePoints]).';

tabRidgenessSphericalnessCyclideness = fscanf(fid,'%f %f %f\n',[3,NombreDeLignes]).';

tabSegments = uint32(fscanf(fid,'%d %d %d\n',[3,NombreDeSegments]).');
tri = tabSegments(:,3);
tabSegments = tabSegments(:,1:2);

fclose(fid);

[tabPoints,index] = sortrows(tabPoints,4);
oldCode = (1:length(index))-1;
[a,index] = ismember(oldCode,index-1);
index = index-1;
[a,b] = ismember(tabSegments,oldCode);
tabSegments(a) = index(b(a));


[a,b] = ismember(tabSegments(:,1),oldCode);
tabSegments(:,3) = tabPoints(b,4);
tabSegments(:,4) = tri;
