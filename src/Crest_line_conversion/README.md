# Crest line conversion

## Getting Started

This project allow to convert ridge and ravine curves obtained from [Crest Lines](http://www.riken.jp/brict/Yoshizawa/Research/Crest.html) in VTK format to be used with the Curve editor.

### Prerequisites

No MATLAB toolbox is needed.

### Installing

In MATLAB, run 
>> ridges_ravines_2vtk_v2(fileridges,fileravines,vtkfile)
where fileridges is the txt files with ridges, fileravines is the txt files with ravines and vtkfile is the surface file in VTK format.

Example:
>> ridges_ravines_2vtk_v2('ridges.txt','ravines.txt','RG9338-skull_endocast.vtk')

see Crest_line_conversion_example directory for a data set.

## Author

* Copyright 2019 [**Jean Dumoncel**](https://gitlab.com/jeandumoncel) 

Contributors (test): 
* Amélie Beaudet
* Edwin de Jager

## License

    If you use this program for your publication, please cite:
      [1] Beaudet, A., Dumoncel, J., de Beer, F., Duployer, B., Durrleman, S., Gilissen, E. P., et al. (2016). 
           Morphoarchitectural variation in South African fossil cercopithecoid endocasts. Journal of Human Evolution,
           101, 65-78. doi:10.1016/j.jhevol.2016.09.003
           
           

