function [] = Write_vtk_polyline_multi_scalar(Pts, Tri,Scalars,Scalars2,Scalars3,filename,name)

% Usage :
%       Write_vtk_polyline(contour.',{tri},'MNHN-ZO-MO-1973-115_URM3_slice_contour.vtk');
% Pts : 3xN
% Copyright modified by Jean Dumoncel


fid = fopen([filename(1:end-4) '.vtk'] ,'wt');
fprintf(fid,'# vtk DataFile Version 3.0\r\nvtk output\r\nASCII\r\nDATASET POLYDATA\r\n');
fprintf(fid,'POINTS %d float\r\n',size(Pts,2));
form = '';
for k=1:size(Pts,1)
    form = sprintf('%s %%f',form);
end
form = [form '\r\n'];

fprintf(fid,form,Pts);

cpt=0;
for k=1:size(Tri,1)
    cpt = cpt + size(Tri{k},2);
end
fprintf(fid,'LINES %d %d\r\n',size(Tri,1),cpt+size(Tri,1));
for k=1:size(Tri,1)
    fprintf(fid,'%d ',size(Tri{k},2));
    for p=1:size(Tri{k},2)
        fprintf(fid,'%d ',Tri{k}(p));
    end
    fprintf(fid,'\r\n');
end

fprintf(fid,'\nCELL_DATA %d\n',length(Tri));

if( ~isempty(Scalars) ) % write the scalars if any
    
    fprintf(fid,'SCALARS %s double\n',name{1});
    fprintf(fid, 'LOOKUP_TABLE default\n');
    fprintf(fid, '%f\n',Scalars);
    
end

if( ~isempty(Scalars2) ) % write the scalars if any
    
    fprintf(fid,'SCALARS %s double\n',name{2});
    fprintf(fid, 'LOOKUP_TABLE default\n');
    fprintf(fid, '%f\n',Scalars2);
    
end

if( ~isempty(Scalars3) ) % write the scalars if any
    
    fprintf(fid,'SCALARS %s double\n',name{3});
    fprintf(fid, 'LOOKUP_TABLE default\n');
    fprintf(fid, '%f\n',Scalars3);
    
end

fclose(fid);
