function [Points, Tri, Normals, Scalars, LUT] = VTKPolyLineReader(filename)

% Usage:
% [Points, Tri, Normals. Scalars, LUT] = VTKPolyDataReader(filename)
% Copyright modified by Jean Dumoncel

fid = fopen(filename, 'r');
if(fid==-1)
    error('Error: file descriptor not valid, check the file name.\n');
    return;
end


keyWord = 'DATASET POLYDATA';
newL = GoToKeyWord(fid, keyWord);
if(newL == -1)
    fprintf(1, 'Error: file is not a vtkPolyData.\n');
    return;
end


% Output:
Points = [];  % set of vertices
Tri = [];     % set of triangles
Normals = []; % set of normals
Scalars = []; % set of scalars
LUT = [];     % LOOKUP Table


newL = fgetl(fid);
keyWord = 'POINTS';
newL = GoToKeyWord(fid, keyWord);
if(newL==-1)
    fprintf(1, 'Cannot find flag: %s\n', keyWord);
end

buffer = sscanf(newL,'%s%d%s');
numPoints = buffer(length(keyWord)+1); % because these are points


Points = fscanf(fid,'%f',[3 numPoints]).';

% Read the polygons
keyWord = 'LINES';
newL = GoToKeyWord(fid, keyWord);
if(newL == -1)
    keyWord = 'POLYGONS';
    newL = GoToKeyWord(fid, keyWord);
    if(newL == -1)
    return;
    end
end
buffer = sscanf(newL, '%s%d%d');
numPoly = buffer(length(keyWord)+1); % get the number of polygons
numTotal = buffer(length(keyWord)+2); % get the actual number of things to read


Tri = fscanf(fid,'%d',[3 numTotal]).'+1;
Tri = Tri.';
Tri = Tri(:);
cpt = 1;
cptCell = 1;
while cpt<numel(Tri)
    nbS = Tri(cpt)-1;
    cpt = cpt+1;
    for k=1:nbS
        TriF{cptCell}(k) = Tri(cpt);
        cpt = cpt+1;
    end
    cptCell = cptCell + 1;
end
Tri = TriF.';

newL = fgetl(fid);
keyWord = 'CELL_DATA';
newL = GoToKeyWord(fid, keyWord);
if(newL~=-1)

buffer = sscanf(newL,'%s%d%s');
numLabels = buffer(length(keyWord)+1); % because these are points

newL = GoToKeyWord(fid, 'LOOKUP_TABLE default');
LUT = uint8(fscanf(fid,'%f',[1 numLabels]).');
end

fclose(fid);
