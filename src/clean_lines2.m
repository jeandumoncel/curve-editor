% License & disclaimer.
% ------------------------------------------------------------------------------
%     Copyright 2019 Dumoncel Jean
%     Modified by Edwin de Jager (2023)
% ------------------------------------------------------------------------------
%  
% IMPORTANT
%     If you use this program for your publication, please cite:
%       [1] Beaudet, A., Dumoncel, J., de Beer, F., Duployer, B., Durrleman, S., Gilissen, E. P., et al. (2016). 
%           Morphoarchitectural variation in South African fossil cercopithecoid endocasts. Journal of Human Evolution,
%           101, 65-78. doi:10.1016/j.jhevol.2016.09.003


function varargout = clean_lines2(varargin)
% clean_lines_Version MATLAB code for clean_lines_Version.fig
%      clean_lines_Version, by itself, creates a new clean_lines_Version or raises the existing
%      singleton*.
%
%      H = clean_lines_Version returns the handle to a new clean_lines_Version or the handle to
%      the existing singleton*.
%
%      clean_lines_Version('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in clean_lines_Version.M with the given input arguments.
%
%      clean_lines_Version('Property','Value',...) creates a new clean_lines_Version or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before clean_lines_Version_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to clean_lines_Version_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help clean_lines_Version

% Last Modified by GUIDE v2.5 19-Jul-2018 15:08:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @clean_lines_Version_OpeningFcn, ...
    'gui_OutputFcn',  @clean_lines_Version_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before clean_lines_Version is made visible.
function clean_lines_Version_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to clean_lines_Version (see VARARGIN)

% Choose default command line output for clean_lines_Version
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes clean_lines_Version wait for user response (see UIRESUME)
% uiwait(handles.figure1);


data = struct;
[FileNameS,PathNameS] = uigetfile('*.vtk','Select the surface file');
[FileNameSr,PathNameSr] = uigetfile(fullfile(PathNameS,'*.vtk'),'Select the reduced surface file');
[FileNameR,PathNameR] = uigetfile(fullfile(PathNameS,'*.vtk'),'Select the ravine/ridge file');
[vertex,face] = read_vtk(fullfile(PathNameS,FileNameS));
data.mesh.vertex = vertex.';
data.mesh.face = face.';
data.PathName = PathNameR;
data.FileName = FileNameR;
[vertexr,facer] = read_vtk(fullfile(PathNameSr,FileNameSr));
data.meshr.vertexr = (vertexr.');
data.meshr.facer = facer.';

data.Lines.colors = [         0         0    1.0000
    1.0000         0         0
    0    1.0000         0
    0         0    0.1724
    1.0000    0.1034    0.7241
    1.0000    0.8276         0
    0    0.3448         0
    0.5172    0.5172    1.0000
    0.6207    0.3103    0.2759
    0    1.0000    0.7586
    0    0.5172    0.5862
    0         0    0.4828
    0.5862    0.8276    0.3103
    0.9655    0.6207    0.8621
    0.8276    0.0690    1.0000
    0.4828    0.1034    0.4138
    0.9655    0.0690    0.3793
    1.0000    0.7586    0.5172
    0.1379    0.1379    0.0345
    0.5517    0.6552    0.4828
    0.9655    0.5172    0.0345
    0.5172    0.4483         0
    0.4483    0.9655    1.0000
    0.6207    0.7586    1.0000
    0.4483    0.3793    0.4828
    0.6207         0         0
    0    0.3103    1.0000
    0    0.2759    0.5862
    0.8276    1.0000         0
    0.7241    0.3103    0.8276
    0.2414         0    0.1034
    0.9310    1.0000    0.6897
    1.0000    0.4828    0.3793
    0.2759    1.0000    0.4828
    0.0690    0.6552    0.3793
    0.8276    0.6552    0.6552
    0.8276    0.3103    0.5172
    0.4138         0    0.7586
    0.1724    0.3793    0.2759
    0    0.5862    0.9655
    0.0345    0.2414    0.3103
    0.6552    0.3448    0.0345
    0.4483    0.3793    0.2414
    0.0345    0.5862         0
    0.6207    0.4138    0.7241
    1.0000    1.0000    0.4483
    0.6552    0.9655    0.7931
    0.5862    0.6897    0.7241
    0.6897    0.6897    0.0345
    0.1724         0    0.3103
    0    0.7931    1.0000
    0.3103    0.1379         0
    0    0.7241    0.6552
    0.6207         0    0.2069
    0.3103    0.4828    0.6897
    0.1034    0.2759    0.7586
    0.3448    0.8276         0
    0.4483    0.5862    0.2069
    0.8966    0.6552    0.2069
    0.9655    0.5517    0.5862
    0.4138    0.0690    0.5517
    0.8966    0.3793    0.7586
    0.9310    0.8276    1.0000
    0.6207    1.0000    0.6207
    1.0000    0.3103    0.9655
    0.5862    0.3793    1.0000
    0.7586    0.7241    0.3793
    0.9310    0.1724    0.2069
    0.6897    0.4138    0.5172
    0.2759    0.1724    0.3448
    0.7241    0.5172    0.3448
    0.5517    0.0345    1.0000
    0.2759    0.3103         0
    0.8621    0.8276    0.6897
    0.8966    0.5862    1.0000
    0         0    0.0345
    0.3793    0.2414    0.2414
    0.9310         0    0.5172
    0.6897         0    0.5862
    0.4138         0         0
    0.7241    0.6552    1.0000
    0.6552    0.5862    0.7586
    0.5172    0.1724    0.3103
    0.4483    0.4828    0.4483
    0.8621    0.2759         0
    0.0345    0.2069         0
    0.5862    0.7931    0.4828
    0    0.0690    0.8276
    0.7241    0.9310    1.0000
    0.3448    0.2414    0.5862
    0.2759    0.5172    0.2759
    0.6897    1.0000    0.3103
    0.2759    0.7931    0.3448
    0.8966    0.3448    0.4138
    0.9310    0.5517    0.2759
    0.4828    0.3103    0.0690
    0.3793    0.4483    0.7931
    0    1.0000    0.9310
    0.3103    0.5862    0.5172
    0.7241         0    0.7931];


data.Lines.colors2 = data.Lines.colors(randperm(100),:);

segment = zeros(3*size(data.mesh.face,1),2);
segment(1:3:end,:) = [data.mesh.face(:,1) data.mesh.face(:,2)];
segment(2:3:end,:) = [data.mesh.face(:,2) data.mesh.face(:,3)];
segment(3:3:end,:) = [data.mesh.face(:,1) data.mesh.face(:,3)];
data.segment = unique(segment, 'rows');
data.DG = sparse([data.segment(:,1);data.segment(:,2)],[data.segment(:,2);data.segment(:,1)],ones(size([data.segment(:,2);data.segment(:,1)],1),1));

[Points, Lines, Normals, Scalars, LUT] = VTKPolyLineReader(fullfile(PathNameR,FileNameR));

aenlever = [];
for k=1:size(Lines,1)
    if length(Lines{k})==1
        aenlever = [aenlever k];
    end
    for p=k+1:size(Lines,1)
        if length(Lines{k})==length(Lines{p})
            if all(Lines{k}==Lines{p})
                aenlever = [aenlever k];
            end
        end
    end
end
Lines(aenlever) = [];
if ~isempty(LUT)
    LUT(aenlever) = [];
end

data.Lines.Points = Points;
data.Lines.Lines = cellfun(@(x) x.',Lines,'UniformOutput',0);
if ~isempty(LUT)
    data.Lines.labelNumber = LUT;
    data.Lines.labelName = cell(size(data.Lines.Lines,1),1);
else
    data.Lines.labelNumber = zeros(size(data.Lines.Lines,1),1);
    data.Lines.labelName = cell(size(data.Lines.Lines,1),1);
end

data.Lines.PointsMesh = zeros(size(data.Lines.Points,1),1);


ind = knnsearch(data.mesh.vertex,data.Lines.Points);
% % % slow version
% % % ind = arrayfun(@(x) find(sqrt(sum((data.mesh.vertex-repmat(data.Lines.Points(x,:),size(data.mesh.vertex,1),1)).^2,2))<0.0001),1:size(data.Lines.Points,1));
data.Lines.PointsMesh = ind;

data.Lines.Segments = [];
for k=1:size(data.Lines.Lines,1)
    for p=1:numel(data.Lines.Lines{k})-1
        data.Lines.Segments(end+1,:) = [data.Lines.Lines{k}(p) data.Lines.Lines{k}(p+1)];
    end
end

inddel = [];
indpoints = [];
for k=1:size(data.Lines.Lines,1)
    if size(data.Lines.Lines{k},1)==1
        inddel = [inddel k];
        indpoints = [indpoints data.Lines.Lines{k}];
    end
end

indpoints = [indpoints setdiff(1:size(data.Lines.Points,1),cell2mat(data.Lines.Lines))];

data.Lines.Lines(inddel) = [];
data.Lines.labelNumber(inddel) = [];
data.Lines.labelName(inddel) = [];


indpointstmp = indpoints;
for k=1:length(indpointstmp)
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k))-1;
    end
    indpointstmp = indpointstmp-1;
end
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];


for k=1:numel(data.Lines.Lines)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end

l = 0;
cpt = 0;
for k=1:numel(data.Lines.Lines)
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
        cpt = cpt+1;
    end
end
data.Lines.meanlength = l/cpt;



setMyData(data)

display_fig(handles)



% --- Outputs from this function are returned to the command line.
function varargout = clean_lines_Version_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% "Remove curve"
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
pause
flag=0;
point = getCursorInfo(dcm);
indLinesTMP = [];
% while ~flag, selection of 1 curve
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        % display selected line in red
        if ~isempty(indLinesTMP)
            hold on
            for k=indLinesTMP
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
            end
            drawnow
        end
        point = getCursorInfo(dcm);
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        indLines = [];
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
        end
        indLinesTMP = indLines;
        hold on
        for k=indLines
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
        end
        drawnow
        pause
    else
        flag = 1;
    end
end



indPoint = find(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end
data.Lines.Lines(indLines) = [];
data.Lines.labelNumber(indLines) = [];
data.Lines.labelName(indLines) = [];

inddel = [];
indpoints = [];
for k =1:size(data.Lines.Lines,1)
    if size(data.Lines.Lines{k},1)==1
        inddel = [inddel k];
        indpoints = [indpoints data.Lines.Lines{k}];
    end
end
data.Lines.Lines(inddel) = [];
indpoints = [indpoints setdiff(1:size(data.Lines.Points,1),cell2mat(data.Lines.Lines))];
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];
for k=length(indpoints):-1:1
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k))-1;
    end
end

data.Lines.Longueurs = [];
for k=1:size(data.Lines.Lines,1)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end



setMyData(data)
v = axis;
display_fig(handles)
axis(v)

% "Remove segment"
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on');
pause

flag=0;
point = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        point = getCursorInfo(dcm);
        dcm = datacursormode(gcf);
        set(dcm,'Enable','on');
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        indLines = [];
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
        end
        indLinesTMP = indLines;
        v = axis;
        display_fig(handles)
        axis(v)
        hold on
        plot3(point.Position(1),point.Position(2),point.Position(3),'o','MarkerSize',20,'Linewidth',3,'Color','b')
        drawnow
        pause
    else
        flag = 1;
    end
end

indPoint = find(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
for k=1:numel(indPoint)
    indSegment = cellfun(@(x) find(x==indPoint(k)),data.Lines.Lines,'UniformOutput',0);
    cpt=1;
    indLines = [];
    
    for p=1:numel(indSegment)
        if ~isempty(indSegment{p})
            curve{cpt} = data.Lines.Lines{p}(1:indSegment{p}-1);
            cpt=cpt+1;
            curve{cpt} = data.Lines.Lines{p}(indSegment{p}+1:end);
            cpt=cpt+1;
            indLines = [indLines p];
        end
    end
    if ~isempty(indLines)
        data.Lines.Lines(indLines) = [];
        for p=1:numel(curve)
            if ~isempty(curve{p})
                data.Lines.Lines{end+1} = curve{p};
                data.Lines.labelNumber(end+1) = data.Lines.labelNumber(indLines);
                data.Lines.labelName{end+1} = data.Lines.labelName{indLines};
            end
        end
        data.Lines.labelNumber(indLines) = [];
        data.Lines.labelName(indLines) = [];
    end
end

inddel = [];
indpoints = [];
for k =1:size(data.Lines.Lines,1)
    if size(data.Lines.Lines{k},1)==1
        inddel = [inddel k];
        indpoints = [indpoints data.Lines.Lines{k}];
    end
end
data.Lines.Lines(inddel) = [];
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];
for k=length(indpoints):-1:1
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k))-1;
    end
end

data.Lines.Longueurs = [];
for k=1:size(data.Lines.Lines,1)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end



setMyData(data)
v = axis;
display_fig(handles)
axis(v)



% "Add lines"
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on');
pause


flag=0;
pointI = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        pointI = getCursorInfo(dcm);
        dcm = datacursormode(gcf);
        set(dcm,'Enable','on');
        v = axis;
        display_fig(handles)
        axis(v)
        hold on
        plot3(pointI.Position(1),pointI.Position(2),pointI.Position(3),'o','MarkerSize',20,'Linewidth',3,'Color','b')
        drawnow
        pause
    else
        flag = 1;
    end
end

pause

flag=0;
pointF = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        pointF = getCursorInfo(dcm);
        dcm = datacursormode(gcf);
        set(dcm,'Enable','on');
        v = axis;
        display_fig(handles)
        axis(v)
        hold on
        plot3(pointF.Position(1),pointF.Position(2),pointF.Position(3),'o','MarkerSize',20,'Linewidth',3,'Color','b')
        plot3(pointI.Position(1),pointI.Position(2),pointI.Position(3),'o','MarkerSize',20,'Linewidth',3,'Color','b')
        drawnow
        pause
    else
        flag = 1;
    end
end

[val ind] = min(sqrt(sum((data.mesh.vertex-repmat(pointI.Position,size(data.mesh.vertex,1),1)).^2,2)));
pointI = data.mesh.vertex(ind,:);

[val ind] = min(sqrt(sum((data.mesh.vertex-repmat(pointF.Position,size(data.mesh.vertex,1),1)).^2,2)));
pointF = data.mesh.vertex(ind,:);

indPointI = find(sqrt(sum((data.Lines.Points-repmat(pointI,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indPointF = find(sqrt(sum((data.Lines.Points-repmat(pointF,size(data.Lines.Points,1),1)).^2,2))<0.0001);
if ~isempty(indPointI) && ~isempty(indPointF)
    
    [dist,path,pred] = graphshortestpath(data.DG,data.Lines.PointsMesh(indPointI),data.Lines.PointsMesh(indPointF));
    if iscell(path)
        path = path{1};
    end
    
    indSegment = cellfun(@(x) find(x==indPointI(1)),data.Lines.Lines,'UniformOutput',0);
    flag=0;
    for p=1:numel(indSegment)
        if ~isempty(indSegment{p})
            indSegment{p} = indSegment{p}(end);
            if indSegment{p}==numel(data.Lines.Lines{p})
                data.Lines.Lines{p} = [data.Lines.Lines{p};(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(2:end-1),:),1)-1).';indPointF];
                data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(2:end-1),:),1)-1,:) = data.mesh.vertex(path(2:end-1),:);
                data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(2:end-1),:),1)-1) = path(2:end-1);
                flag = 1;
            else
                data.Lines.Lines{p} = [indPointF;(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1).'; data.Lines.Lines{p}];
                data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1,:) = data.mesh.vertex(path(end-1:-1:2),:);
                data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1) = path(end-1:-1:2);
                flag=1;
            end
        end
    end
    if ~flag && length(indPointI)==2
        indSegment = cellfun(@(x) find(x==indPointI(2)),data.Lines.Lines,'UniformOutput',0);
        for p=1:numel(indSegment)
            if ~isempty(indSegment{p})
                indSegment{p} = indSegment{p}(end);
                if indSegment{p}==numel(data.Lines.Lines{p})
                    data.Lines.Lines{p} = [data.Lines.Lines{p};(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(2:end-1),:),1)-1).';indPointF];
                    data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(2:end-1),:),1)-1,:) = data.mesh.vertex(path(2:end-1),:);
                    data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(2:end-1),:),1)-1) = path(2:end-1);
                else
                    data.Lines.Lines{p} = [indPointF;(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1).'; data.Lines.Lines{p}];
                    data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1,:) = data.mesh.vertex(path(end-1:-1:2),:);
                    data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(end-1:-1:2),:),1)-1) = path(end-1:-1:2);
                    
                end
            end
        end
        
    end
    
elseif ~isempty(indPointI) && isempty(indPointF)
    test = inf;
    Nb = 0;
    while Nb <100 && test>5*data.Lines.meanlength
        indPointF = find(sqrt(sum((data.mesh.vertex-repmat(pointF,size(data.mesh.vertex,1),1)).^2,2))<0.0001);
        [dist,path,pred] = graphshortestpath(data.DG,data.Lines.PointsMesh(indPointI),indPointF,'Directed',false,'Method','BFS');
        [dist,path,pred] = graphshortestpath(data.DG,data.Lines.PointsMesh(indPointI),indPointF);
        test = max(sqrt(sum((data.mesh.vertex(path(2:end),:)-data.mesh.vertex(path(1:end-1),:)).^2,2)));
    end
    if Nb==100
        msgbox('Erreur dans la recherche du chemin');
        error('Path search error');
    end
    if Nb>1
        msgbox(sprintf('La recherche du chemin a ?t? recommenc? %d fois',Nb));
    end
    
    
    indSegment = cellfun(@(x) find(x==indPointI(1)),data.Lines.Lines,'UniformOutput',0);
    flag=0;
    for p=1:numel(indSegment)
        if ~isempty(indSegment{p})
            if indSegment{p}==numel(data.Lines.Lines{p})
                data.Lines.Lines{p} = [data.Lines.Lines{p};(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(2:end),:),1)-1).'];
                data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(2:end),:),1)-1,:) = data.mesh.vertex(path(2:end),:);
                data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(2:end),:),1)-1) = path(2:end);
                flag = 1;
            else
                data.Lines.Lines{p} = [(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1).'; data.Lines.Lines{p}];
                data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1,:) = data.mesh.vertex(path(end:-1:2),:);
                data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1) = path(end:-1:2);
                flag = 1;
            end
        end
    end
    if ~flag && length(indPointI)==2
        indSegment = cellfun(@(x) find(x==indPointI(2)),data.Lines.Lines,'UniformOutput',0);
        for p=1:numel(indSegment)
            if ~isempty(indSegment{p})
                if indSegment{p}==numel(data.Lines.Lines{p})
                    data.Lines.Lines{p} = [data.Lines.Lines{p};(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(2:end),:),1)-1).'];
                    data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(2:end),:),1)-1,:) = data.mesh.vertex(path(2:end),:);
                    data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(2:end),:),1)-1) = path(2:end);
                else
                    data.Lines.Lines{p} = [(size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1).'; data.Lines.Lines{p}];
                    data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1,:) = data.mesh.vertex(path(end:-1:2),:);
                    data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(end:-1:2),:),1)-1) = path(end:-1:2);
                end
            end
        end
    end
    
elseif isempty(indPointI) && isempty(indPointF)
    indPointI = find(sqrt(sum((data.mesh.vertex-repmat(pointI,size(data.mesh.vertex,1),1)).^2,2))<0.0001);
    indPointF = find(sqrt(sum((data.mesh.vertex-repmat(pointF,size(data.mesh.vertex,1),1)).^2,2))<0.0001);
    [dist,path,pred] = graphshortestpath(data.DG,indPointI,indPointF);
    data.Lines.Lines{end+1} = (size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(path(1:end),:),1)-1).';
    data.Lines.Points(end+1:end+1+size(data.mesh.vertex(path(1:end),:),1)-1,:) = data.mesh.vertex(path(1:end),:);
    data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(path(1:end),:),1)-1) = path(1:end);
    data.Lines.labelNumber(end+1) = 0;
    data.Lines.labelName{end+1} = '';
end

inddel = [];
indpoints = [];
for k =1:size(data.Lines.Lines,1)
    if size(data.Lines.Lines{k},1)==1
        inddel = [inddel k];
        indpoints = [indpoints data.Lines.Lines{k}];
    end
end
data.Lines.Lines(inddel) = [];
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];
for k=1:length(indpoints)
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpoints(k))-1;
    end
end

data.Lines.Longueurs = [];
for k=1:size(data.Lines.Lines,1)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end


setMyData(data)
v = axis;
display_fig(handles)
axis(v)

function setMyData(data)
% Store data struct in figure
data_k1=getappdata(gcf,'data3d');
setappdata(gcf,'data3dk1',data_k1);
setappdata(gcf,'data3d',data);

function data=getMyData()
% Get data struct stored in figure
data=getappdata(gcf,'data3d');


function display_fig(handles)
data=getMyData();
SurfaceState = get(handles.pushbutton5, 'String');
if strcmp(SurfaceState,'Hide surface')
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    title('Curves to edit')
else
    cla
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    axis auto
    axis equal
    camlight
    hold on
    title('Curves to edit')
end
DirectionsState = get(handles.pushbutton14, 'String');
if strcmp(DirectionsState,'Hide Labels')
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color',data.Lines.colors(data.Lines.labelNumber(k)+1,:));
    end
    title('Curves to edit')
end


% "Save"
% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
[FileName,PathName] = uiputfile(fullfile(data.PathName,'*.vtk'),'Curves',[fullfile(data.PathName,data.FileName(1:end-4))  '_curves']);
fid = fopen([fullfile(PathName,FileName) '.vtk'] ,'wt');
fprintf(fid,'# vtk DataFile Version 3.0\r\nvtk output\r\nASCII\r\nDATASET POLYDATA\r\n');
fprintf(fid,'POINTS %d float\r\n',size(data.Lines.Points,1));
for k=1:size(data.Lines.Points,1)
    fprintf(fid,'%f %f %f\r\n',data.Lines.Points(k,1),data.Lines.Points(k,2),data.Lines.Points(k,3));
end
cpt=0;
for k=1:numel(data.Lines.Lines)
    cpt = cpt + numel(data.Lines.Lines{k});
end
fprintf(fid,'LINES %d %d\r\n',numel(data.Lines.Lines),cpt+numel(data.Lines.Lines));
for k=1:numel(data.Lines.Lines)
    fprintf(fid,'%d ',numel(data.Lines.Lines{k}));
    for p=1:numel(data.Lines.Lines{k})
        fprintf(fid,'%d ',data.Lines.Lines{k}(p)-1);
    end
    fprintf(fid,'\r\n');
end
fprintf(fid,'CELL_DATA %d\n',numel(data.Lines.Lines));
fprintf(fid,'SCALARS scalars double\n');
fprintf(fid, 'LOOKUP_TABLE default\n');
fprintf(fid, '%f\n',data.Lines.labelNumber);
fclose(fid);


fid = fopen(fullfile(PathName,sprintf('%s.lineset',FileName)) ,'wt');
fprintf(fid,'# LineSet 0.1\n\nnDataVals 1\n\n{ ');
for k=1:numel(data.Lines.Lines)
    fprintf(fid,'{\n');
    for p=1:numel(data.Lines.Lines{k})
        fprintf(fid,'%f %f %f %f\n',data.Lines.Points(data.Lines.Lines{k}(p),1),data.Lines.Points(data.Lines.Lines{k}(p),2),data.Lines.Points(data.Lines.Lines{k}(p),3),k);
    end
    
    fprintf(fid,'} ');
end
fprintf(fid,'\n}');
fclose(fid);


% "Hide Surface"
% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
SurfaceState = get(handles.pushbutton5, 'String');
if strcmp(SurfaceState,'Hide surface')
    cla
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    axis auto
    axis equal
    camlight
    hold on
    title('Curves to edit')
    set(handles.pushbutton5, 'String','Show surface');
else
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    title('Curves to edit')
    set(handles.pushbutton5, 'String','Hide surface');
end
setMyData(data)

% "Show directions"
% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
DirectionsState = get(handles.pushbutton6, 'String');
if strcmp(DirectionsState,'Show directions')
    cla
    for k=1:numel(data.Lines.Lines)
        pts = data.Lines.Points(data.Lines.Lines{k},:);
        quiver3(pts(1:end-1,1),pts(1:end-1,2),pts(1:end-1,3),pts(2:end,1)-pts(1:end-1,1),pts(2:end,2)-pts(1:end-1,2),pts(2:end,3)-pts(1:end-1,3),'LineWidth',2)
    end
    axis auto
    axis equal
    camlight
    hold on
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    title('Curves to edit')
    set(handles.pushbutton6, 'String','Hide directions');
else
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    title('Curves to edit')
    set(handles.pushbutton6, 'String','Show directions');
end
setMyData(data)

% "Save curve"
% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
pause
data=getMyData();
flag=0;
point = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        if ~isempty(indLinesTMP)
            hold on
            for k=indLinesTMP
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
            end
            drawnow
        end
        point = getCursorInfo(dcm);
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        indLines = [];
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
        end
        indLinesTMP = indLines;
        hold on
        for k=indLines
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
        end
        drawnow
        pause
    else
        flag = 1;
    end
end

indPoint = find(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end

ptsL = data.Lines.Points(data.Lines.Lines{indLines},:);

[FileName,PathName] = uiputfile(fullfile(data.PathName,'*.vtk'),'Curve',[fullfile(data.PathName,data.FileName(1:end-4)) '_curve']);
[pathstr, FileName, ext] = fileparts(FileName);
fid = fopen([fullfile(PathName,FileName) '.vtk'] ,'wt');
fprintf(fid,'# vtk DataFile Version 3.0\r\nvtk output\r\nASCII\r\nDATASET POLYDATA\r\n');
fprintf(fid,'POINTS %d float\r\n',size(ptsL,1));
for k=1:size(ptsL,1)
    fprintf(fid,'%f %f %f\r\n',ptsL(k,1),ptsL(k,2),ptsL(k,3));
end
cpt = size(ptsL,1);
fprintf(fid,'LINES %d %d\r\n',cpt,cpt+1);
for k=1
    fprintf(fid,'%d ',cpt);
    for p=1:cpt
        fprintf(fid,'%d ',p-1);
    end
    fprintf(fid,'\r\n');
end

fclose(fid);
fid = fopen(fullfile(PathName,sprintf('%s.lineset',FileName)) ,'wt');

fprintf(fid,'# LineSet 0.1\n\nnDataVals 1\n\n{ ');

for k=1
    fprintf(fid,'{\n');
    for p=1:size(ptsL,1)
        fprintf(fid,'%f %f %f %f\n',ptsL(p,1),ptsL(p,2),ptsL(p,3),k);
    end
    
    fprintf(fid,'} ');
end

fprintf(fid,'\n}');
fclose(fid);

% "Reverse Curve"
% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on');
pause
flag=0;
point = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        if ~isempty(indLinesTMP)
            hold on
            for k=indLinesTMP
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
            end
            drawnow
        end
        point = getCursorInfo(dcm);
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        indLines = [];
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
        end
        indLinesTMP = indLines;
        hold on
        for k=indLines
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
        end
        drawnow
        pause
    else
        flag = 1;
    end
end



indPoint = find(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end
data.Lines.Lines{indLines} = data.Lines.Lines{indLines}(end:-1:1);

data.Lines.Longueurs = [];
for k=1:size(data.Lines.Lines,1)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end


setMyData(data)
v = axis;
display_fig(handles)
axis(v)


% Merge Curves
% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
pause
point1 = getCursorInfo(dcm);
pause
point2 = getCursorInfo(dcm);
indPoint = find(sqrt(sum((data.Lines.Points-repmat(point1.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end
pts = data.Lines.Points(data.Lines.Lines{indLines},:);
data.Lines.Lines(indLines) = [];

indPoint = find(sqrt(sum((data.Lines.Points-repmat(point2.Position,size(data.Lines.Points,1),1)).^2,2))<0.0001);
indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end
pts = [pts;data.Lines.Points(data.Lines.Lines{indLines},:)];
data.Lines.Lines(indLines) = [];

indPoint1 = zeros(size(pts,1),1);
for k=1:size(pts,1)
    indPoint1(k) = find(sqrt(sum((data.mesh.vertex-repmat(pts(k,:),size(data.mesh.vertex,1),1)).^2,2))<0.0001);
end

data.Lines.Lines{end+1} = (size(data.Lines.Points,1)+1:size(data.Lines.Points,1)+1+size(data.mesh.vertex(indPoint1,:),1)-1).';
data.Lines.Points(end+1:end+1+size(data.mesh.vertex(indPoint1,:),1)-1,:) = data.mesh.vertex(indPoint1,:);
data.Lines.PointsMesh(end+1:end+1+size(data.mesh.vertex(indPoint1,:),1)-1) = indPoint1;
data.Lines.labelNumber(end+1) = data.Lines.labelNumber(indLines(1));
data.Lines.labelName{end+1} = data.Lines.labelName{indLines(1)};
data.Lines.labelNumber(indLines) = [];
data.Lines.labelName(indLines) = [];

data.Lines.Longueurs = [];
for k=1:size(data.Lines.Lines,1)
    l = 0;
    for p=2:size(data.Lines.Lines{k},1)
        l = l + sqrt(sum((data.Lines.Points(data.Lines.Lines{k}(p),:)-data.Lines.Points(data.Lines.Lines{k}(p-1),:)).^2));
    end
    data.Lines.Longueurs{k} = l;
end

setMyData(data)
v = axis;
display_fig(handles)
axis(v)

% "Delete curves (distance)"
% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
sortie = 1;
while sortie
    prompt = {'Enter value:'};
    dlg_title = 'Selection';
    num_lines = 1;
    def = {'5'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    if isempty(answer)
        sortie = 0;
    else
        ind = [];
        for k=1:size(data.Lines.Longueurs,2)
            if data.Lines.Longueurs{k}>str2num(answer{1})
                ind = [ind k];
            end
        end
        
        indI = setdiff(1:size(data.Lines.Longueurs,2),ind);
        
        v = axis;
        cla
        patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
        axis auto
        axis equal
        camlight
        hold on
        for k=ind
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
        end
        for k=indI
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4);
        end
        title('Curves to edit')
        axis(v)
        drawnow
        
        h = msgbox('Visualisation')
        waitfor(h)
        options.Interpreter = 'tex';
        options.Default = 'Don''t know';
        options.WindowStyle = 'normal';
        % Create a TeX string for the question
        qstring = 'Delete blue curves?';
        choice = questdlg(qstring,'Deleting the selection...',...
            'Yes','Cancel','Don''t know',options);
        switch choice
            case 'Yes'
                data.Lines.Lines(indI) = [];
                data.Lines.labelNumber(indI) = [];
                data.Lines.labelName(indI) = [];
                indpoints = setdiff(1:size(data.Lines.Points,1),cell2mat(data.Lines.Lines));
                indpointstmp = indpoints;
                for k=length(indpointstmp):-1:1
                    for p=1:size(data.Lines.Lines,1)
                        data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k))-1;
                    end
                    indpointstmp = indpointstmp-1;
                end
                data.Lines.Points(indpoints,:) = [];
                data.Lines.PointsMesh(indpoints) = [];
                data.Lines.Longueurs(indI) = [];
                setMyData(data)
                v = axis;
                display_fig(handles)
                axis(v)
                sortie = 0;
            case 'Cancel'
                v = axis;
                display_fig(handles)
                axis(v)
                drawnow
                sortie = 0;
            case 'Don''t know'
        end
    end
    
end



% "Select curves"
% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
sortie = 1;
points = [];
indLines = [];
while sortie
    pause
    flag=0;
    pt = getCursorInfo(dcm);
    indLinesTMP = [];
    while ~flag
        currkey=get(gcf,'CurrentKey');
        if strcmp(currkey, 'f')
            if ~isempty(indLinesTMP)
                hold on
                for k=indLinesTMP
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
                end
                drawnow
            end
            pt = getCursorInfo(dcm);
            [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(pt.Position,size(data.Lines.Points,1),1)).^2,2)));
            pt.Position = data.Lines.Points(indPoint,:);
            indLinesTMP = [];
            for k=1:numel(indPoint)
                indLinesTMP = [indLinesTMP find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
            end
            
            hold on
            for k=indLinesTMP
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
            end
            drawnow
            pause
        else
            flag = 1;
        end
    end
    tmp = getCursorInfo(dcm);
    points(end+1,:) = tmp.Position;
    [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(points(end,:),size(data.Lines.Points,1),1)).^2,2)));
    point.Position = data.Lines.Points(indPoint,:);
    for k=1:numel(indPoint)
        indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
    end
    hold on
    cpt = 0;
    for k=indLines
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color',data.Lines.colors2(mod(cpt,size(data.Lines.colors2,1))+1,:));
        cpt = cpt+1;
    end
    drawnow
    
    if size(points,1)>1
        if sqrt(sum((points(end,:)-points(end-1,:)).^2))<0.0001
            sortie=0;
        end
    end
end

data.Lines.Lines = data.Lines.Lines(unique(indLines));
data.Lines.labelNumber = data.Lines.labelNumber(unique(indLines));
data.Lines.labelName = data.Lines.labelName(unique(indLines));

indpoints = setdiff(1:size(data.Lines.Points,1),cell2mat(data.Lines.Lines));
indpointstmp = indpoints;
for k=1:length(indpointstmp)
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k))-1;
    end
    indpointstmp = indpointstmp-1;
end
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];
data.Lines.Longueurs = data.Lines.Longueurs(unique(indLines));
setMyData(data)
v = axis;
display_fig(handles)
axis(v)
drawnow

% "Delete curves (selection)"
% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
sortie = 1;
points = [];
indLines = [];
while sortie
    pause
    flag=0;
    pt = getCursorInfo(dcm);
    indLinesTMP = [];
    while ~flag
        currkey=get(gcf,'CurrentKey');
        if strcmp(currkey, 'f')
            if ~isempty(indLinesTMP)
                hold on
                for k=indLinesTMP
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
                end
                drawnow
            end
            pt = getCursorInfo(dcm);
            [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(pt.Position,size(data.Lines.Points,1),1)).^2,2)));
            pt.Position = data.Lines.Points(indPoint,:);
            indLinesTMP = [];
            for k=1:numel(indPoint)
                indLinesTMP = [indLinesTMP find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
            end
            
            hold on
            for k=indLinesTMP
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
            end
            drawnow
            pause
        else
            flag = 1;
        end
    end

    tmp = getCursorInfo(dcm);
    points(end+1,:) = tmp.Position;
    
    if size(points,1)>1
        if sqrt(sum((points(end,:)-points(end-1,:)).^2))<0.0001
            sortie=0;
        end
    end
    if sortie
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(points(end,:),size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines)).'];
        end
        hold on
        cpt = 1;
        for k=indLines
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color',data.Lines.colors2(mod(cpt,size(data.Lines.colors2,1))+1,:));
            cpt = cpt+1;
        end
        drawnow
    end
end
indI = setdiff(1:size(data.Lines.Lines,1),indLines);
data.Lines.Lines = data.Lines.Lines(indI);
data.Lines.labelNumber = data.Lines.labelNumber(indI);
data.Lines.labelName = data.Lines.labelName(indI);

indpoints = setdiff(1:size(data.Lines.Points,1),cell2mat(data.Lines.Lines));
indpointstmp = indpoints;
for k=1:length(indpointstmp)
    for p=1:size(data.Lines.Lines,1)
        data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k)) = data.Lines.Lines{p}(data.Lines.Lines{p}>indpointstmp(k))-1;
    end
    indpointstmp = indpointstmp-1;
end
data.Lines.Points(indpoints,:) = [];
data.Lines.PointsMesh(indpoints) = [];

data.Lines.Longueurs = data.Lines.Longueurs(indI);

setMyData(data)
v = axis;
display_fig(handles)
axis(v)
drawnow

% "NEW : Go back c"
% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_k1=getappdata(gcf,'data3dk1');
setappdata(gcf,'data3d',data_k1);
v = axis;
display_fig(handles)
axis(v)
drawnow


% "Show Labels"
% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
DirectionsState = get(handles.pushbutton14, 'String');
if strcmp(DirectionsState,'Show Labels')
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color',data.Lines.colors(data.Lines.labelNumber(k)+1,:));
    end
    title('Curves to edit')
    set(handles.pushbutton14, 'String','Hide Labels');
    
else
    cla
    patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
    axis auto
    axis equal
    camlight
    hold on
    for k=1:numel(data.Lines.Lines)
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
    end
    title('Curves to edit')
    set(handles.pushbutton14, 'String','Show Labels');
    
end
setMyData(data);



% "Edit Labels"
% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
pause
flag=0;
point = getCursorInfo(dcm);
indLinesTMP = [];
while ~flag
    currkey=get(gcf,'CurrentKey');
    if strcmp(currkey, 'f')
        if ~isempty(indLinesTMP)
            hold on
            for k=indLinesTMP
                DirectionsState = get(handles.pushbutton14, 'String');
                if strcmp(DirectionsState,'Hide Labels')
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
                else
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
                end
            end
            drawnow
        end
        point = getCursorInfo(dcm);
        [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(point.Position,size(data.Lines.Points,1),1)).^2,2)));
        point.Position = data.Lines.Points(indPoint,:);
        indLines = [];
        for k=1:numel(indPoint)
            indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
        end
        indLinesTMP = indLines;
        hold on
        for k=indLines
            DirectionsState = get(handles.pushbutton14, 'String');
            if strcmp(DirectionsState,'Hide Labels')
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','g');
            else
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
            end
        end
        drawnow
        pause
    else
        flag = 1;
    end
end

indLines = [];
for k=1:numel(indPoint)
    indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
end

prompt = {'Enter curve label number:','Enter curve name:'};
dlg_title = 'Edit label';
num_lines = 1;
if isempty(data.Lines.labelName{indLines})
    defaultans = {num2str(data.Lines.labelNumber(indLines)),''};
else
    defaultans = {num2str(data.Lines.labelNumber(indLines)),data.Lines.labelName{indLines}};
end
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
data.Lines.labelNumber(indLines) = str2num(answer{1});
data.Lines.labelName{indLines} = answer{2};
setMyData(data)
v = axis;
display_fig(handles)
axis(v)

% "Show Label Table"
% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
d = zeros(size(data.Lines.Lines,1),1);
for k=1:size(data.Lines.Lines,1)
    d(k) = length(data.Lines.Lines{k});
end
f = figure('Position', [100 100 400 250]);
t = uitable('Parent', f, 'Position', [25 50 350 200], 'Data', [num2cell(data.Lines.labelNumber) data.Lines.labelName,num2cell(d)]);
t.ColumnName = {'Label Number', 'Curve Name','Number of points'};

% "Edit label (multiple)"
% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
dcm = datacursormode(gcf);
set(dcm,'Enable','on')
sortie = 1;
points = [];
indLines = [];
while sortie
    pause
    
    flag=0;
    pt = getCursorInfo(dcm);
    indLinesTMP = [];
    while ~flag
        currkey=get(gcf,'CurrentKey');
        if strcmp(currkey, 'f')
            if ~isempty(indLinesTMP)
                hold on
                for k=indLinesTMP
                DirectionsState = get(handles.pushbutton14, 'String');
                if strcmp(DirectionsState,'Hide Labels')
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
                else
                    plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','r');
                end
            end
                drawnow
            end
            pt = getCursorInfo(dcm);
            [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(pt.Position,size(data.Lines.Points,1),1)).^2,2)));
            pt.Position = data.Lines.Points(indPoint,:);
            indLinesTMP = [];
            for k=1:numel(indPoint)
                indLinesTMP = [indLinesTMP find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
            end
            
            hold on
            for k=indLinesTMP
            DirectionsState = get(handles.pushbutton14, 'String');
            if strcmp(DirectionsState,'Hide Labels')
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','g');
            else
                plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color','b');
            end
        end
            drawnow
            pause
        else
            flag = 1;
        end
    end
    
    tmp = getCursorInfo(dcm);
    points(end+1,:) = tmp.Position;
    [valtmp indPoint] = min(sqrt(sum((data.Lines.Points-repmat(points(end,:),size(data.Lines.Points,1),1)).^2,2)));
    point.Position = data.Lines.Points(indPoint,:);
    for k=1:numel(indPoint)
        indLines = [indLines find(cellfun(@(x) ~isempty(find(x==indPoint(k))),data.Lines.Lines))];
    end
    hold on
    cpt = 1;
    for k=indLines
        plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'LineWidth',4,'Color',data.Lines.colors2(mod(cpt,size(data.Lines.colors2,1))+1,:));
        cpt = cpt+1;
    end
    drawnow
    
    if size(points,1)>1
        if sqrt(sum((points(end,:)-points(end-1,:)).^2))<0.0001
            sortie=0;
        end
    end
end

prompt = {'Enter curve label number:','Enter curve name:'};
dlg_title = 'Edit label';
num_lines = 1;
defaultans = {num2str(1),''};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
data.Lines.labelNumber(indLines) = str2num(answer{1});
for k=indLines
    data.Lines.labelName{k} = answer{2};
end
setMyData(data)
v = axis;
display_fig(handles)
axis(v)



% "Select curves (labels)"
% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getMyData();
sortie = 1;
while sortie
    prompt = {'Enter value:'};
    dlg_title = 'Selection';
    num_lines = 1;
    def = {'1'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);%,options);
    
    if isempty(answer)
        sortie = 0;
    else
        ind = [];
        for k=1:size(data.Lines.labelNumber,1)
            if data.Lines.labelNumber(k)==str2num(answer{1})
                ind = [ind k];
            end
        end
        indI = setdiff(1:size(data.Lines.Longueurs,2),ind);
        v = axis;
        cla
        patch('Faces',data.meshr.facer,'Vertices',data.meshr.vertexr,'FaceColor','w','EdgeColor','none');
        axis auto
        axis equal
        camlight
        hold on
        for k=ind
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'r','LineWidth',4);
        end
        for k=indI
            plot3(data.Lines.Points(data.Lines.Lines{k},1),data.Lines.Points(data.Lines.Lines{k},2),data.Lines.Points(data.Lines.Lines{k},3),'b','LineWidth',4);
        end
        title('Curves to edit')
        axis(v)
        drawnow
        
        % Display the dialog box with multiple options
        choice = questdlg('Select an action:', 'Action Selection', 'Delete Curves', 'Edit Label Number', 'Cancel', 'Delete Curves');
        switch choice
            case 'Delete Curves'
                data.Lines.Lines = data.Lines.Lines(unique(ind));
                data.Lines.labelNumber = data.Lines.labelNumber(unique(ind));
                data.Lines.labelName = data.Lines.labelName(unique(ind));

                indpoints = setdiff(1:size(data.Lines.Points, 1), cell2mat(data.Lines.Lines));
                indpointstmp = indpoints;
                for k = 1:length(indpointstmp)
                    for p = 1:size(data.Lines.Lines, 1)
                        data.Lines.Lines{p}(data.Lines.Lines{p} > indpointstmp(k)) = data.Lines.Lines{p}(data.Lines.Lines{p} > indpointstmp(k)) - 1;
                    end
                    indpointstmp = indpointstmp - 1;
                end
                data.Lines.Points(indpoints, :) = [];
                data.Lines.PointsMesh(indpoints) = [];

                setMyData(data)
                v = axis;
                display_fig(handles)
                axis(v)
                drawnow
                sortie = 0;
            case 'Edit Label Number'
                newLabelNumber = inputdlg('Enter the new label number:', 'Edit Label Number', 1, {num2str(data.Lines.labelNumber(ind(1)))});
                if ~isempty(newLabelNumber)
                    newLabelNumber = str2num(newLabelNumber{1});
                    data.Lines.labelNumber(ind) = newLabelNumber;

                    setMyData(data)
                    v = axis;
                    display_fig(handles)
                    axis(v)
                    drawnow
                end
            case 'Cancel'
                v = axis;
                display_fig(handles)
                axis(v)
                drawnow
                sortie = 0;
        end
    end
end





