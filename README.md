# Curve editor

## Getting Started

This project contains a MATLAB function (clean_lines2) to edit ridge and ravine lines on a surface. The curve and surface files are in VTK format (see "example" directory)

![IDE_example](/images/Curve_editor_interface.png)

Ridge and ravine lines can be generated using [Crest Lines](http://www.riken.jp/brict/Yoshizawa/Research/Crest.html) 

### Prerequisites

Some MATLAB toolboxes are needed:
1. Statistics and Machine Learning Toolbox
1. Bioinformatics Toolbox

### Installing

In MATLAB, run clean_lines2
1. Select the original surface file.
1. Select the reduced surface file.
1. Select the curve file.

See Curve_editor_manual.pdf for more details and Curve_editor_example directory for a data set (the endocranium file has been extracted from http://www.metafro.be/primates/downloads).

## Author

* Copyright 2019 [**Jean Dumoncel**](https://gitlab.com/jeandumoncel) 

## Developers
* [**Jean Dumoncel**](https://gitlab.com/jeandumoncel) 
* Edwin de Jager

##  Contributors (test): 
* Amélie Beaudet
* Didier Ginibriere
* Edwin de Jager

## License

    If you use this program for your publication, please cite:
      [1] Beaudet, A., Dumoncel, J., de Beer, F., Duployer, B., Durrleman, S., Gilissen, E. P., et al. (2016). 
           Morphoarchitectural variation in South African fossil cercopithecoid endocasts. Journal of Human Evolution,
           101, 65-78. doi:10.1016/j.jhevol.2016.09.003
           
           
Copyright [2019] [Jean Dumoncel]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
